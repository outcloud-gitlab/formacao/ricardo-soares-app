output "primary_blob_connection_string" {
  description = "Primary blob connection string of the Storage Account"
  sensitive   = true
  value       = data.terraform_remote_state.storage_account.outputs.primary_blob_connection_string
}

output "app_url" {
  description = "URL of the react app created in azure"
  value       = data.terraform_remote_state.storage_account.outputs.app_url
}